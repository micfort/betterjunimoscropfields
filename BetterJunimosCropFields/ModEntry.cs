﻿using BetterJunimos;
using StardewModdingAPI;
using StardewModdingAPI.Events;
using StardewValley;

namespace BetterJunimosCropFields
{
    public class ModEntry: Mod
    {
        private CropFieldsAbility? _cropFieldsAbility;

        public override void Entry(IModHelper helper)
        {
            _cropFieldsAbility = new CropFieldsAbility(Monitor);
            helper.Events.GameLoop.GameLaunched += OnLaunched;
            Helper.Events.GameLoop.UpdateTicking += _cropFieldsAbility.UpdateTick;
        }
        
        private void OnLaunched(object? sender, GameLaunchedEventArgs e)
        {
            if (Helper.ModRegistry.GetApi("hawkfalcon.BetterJunimos") is not BetterJunimosApi bjApi)
            {
                Monitor.Log($"Better Junimos is not installed", LogLevel.Error);
                return;
            }
            bjApi.RegisterJunimoAbility(_cropFieldsAbility);
        }
    }
}